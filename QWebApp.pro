#-------------------------------------------------
#
# Project created by QtCreator 2014-11-17T15:47:41
#
#-------------------------------------------------

QT       += core gui webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qwebapp
TEMPLATE = app


SOURCES += src/code/main.cpp\
        src/code/mainwindow.cpp

HEADERS  += src/include/mainwindow.h

FORMS    += src/form/mainwindow.ui

unix {
target.path = /usr/bin/
INSTALLS += target
}